//
//  FomratoValoracionP1ViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/9/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class FomratoValoracionP1ViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {

    @IBOutlet weak var campoNombre: UITextField!
    @IBOutlet weak var campoApellido: UITextField!
    @IBOutlet weak var campoFechaNacimiento: UITextField!
    @IBOutlet weak var campoDireccion: UITextField!
    @IBOutlet weak var campoCodigoPostal: UITextField!
    @IBOutlet weak var campoEstado: UITextField!
    @IBOutlet weak var campoCiudad: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let defaults = UserDefaults.standard
    var datePicker: UIDatePicker = UIDatePicker()
    var tecladoOrigin: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepararUI()
        cargarDatosGuardados()
    }
    
    @IBAction func quickGo(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let fomratoValoracionP2VC = storyboard.instantiateViewController(withIdentifier: "fomratoValoracionP2VC")
        
        self.navigationController?.pushViewController(fomratoValoracionP2VC, animated: true)
    }
    
    //MARK: - funciones
    
    func prepararUI(){
        
        let calendario = Calendar(identifier: .gregorian)
        let fechaActual = Date()
        var componentes = DateComponents()
        var fechaMaxima: Date = Date()
        
        componentes.calendar = calendario
        componentes.year = -19
        componentes.month = 12
        
        fechaMaxima = calendario.date(byAdding: componentes, to: fechaActual)!
        
        if #available(iOS 14.0, *) {
            datePicker.preferredDatePickerStyle = .inline
        }
        
        datePicker.datePickerMode = .date
        campoFechaNacimiento.inputView = datePicker
        
        datePicker.addTarget(self, action: #selector(cambioFechaNacimiento), for: .valueChanged)
        datePicker.maximumDate = fechaMaxima
        
        NotificationCenter.default.addObserver(self, selector: #selector(obtenerTecladoSize(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    func cargarDatosGuardados(){
        
        campoNombre.text = defaults.string(forKey: "nombre")
        campoNombre.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        campoNombre.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)

        campoApellido.text = defaults.string(forKey: "apellido")
        campoApellido.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        campoApellido.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
    }
    
    @objc func cambioFechaNacimiento(){
        
        let fecha = Calendar.current.dateComponents([.day, .month, .year], from: datePicker.date)

        var fechaString = "\(fecha.day ?? 0)"
        
        switch fecha.month {
        case 1:
            fechaString = "\(fechaString) ene"
        case 2:
            fechaString = "\(fechaString) feb"
        case 3:
            fechaString = "\(fechaString) mar"
        case 4:
            fechaString = "\(fechaString) abr"
        case 5:
            fechaString = "\(fechaString) may"
        case 6:
            fechaString = "\(fechaString) jun"
        case 7:
            fechaString = "\(fechaString) jul"
        case 8:
            fechaString = "\(fechaString) ago"
        case 9:
            fechaString = "\(fechaString) sep"
        case 10:
            fechaString = "\(fechaString) oct"
        case 11:
            fechaString = "\(fechaString) nov"
        case 12:
            fechaString = "\(fechaString) dic"
        default:
            fechaString = ""
        }
        
        fechaString = "\(fechaString) \(fecha.year ?? 0)"
        
        campoFechaNacimiento.text = fechaString
        
        campoFechaNacimiento.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        campoFechaNacimiento.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
    }
    
    @objc func obtenerTecladoSize(notification: Notification){

        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            
            return
        }
        
        tecladoOrigin = keyboardRect.height
        
        if tecladoOrigin != 0.0{
            
            if(campoDireccion.isFirstResponder){

                self.view.frame.origin.y = -100
            }else if(campoCodigoPostal.isFirstResponder){
                
                self.view.frame.origin.y = -150
            }else if(campoEstado.isFirstResponder){
                
                self.view.frame.origin.y = -200
            }else if(campoCiudad.isFirstResponder){
                
                self.view.frame.origin.y = -250
            }
        }
        
    }
    
    func obtenerFechaFormateada(fechaDatePicker: UIDatePicker) -> String{
        
        let fecha = Calendar.current.dateComponents([.day, .month, .year], from: fechaDatePicker.date)
        
        var fechaString = "\(fecha.day ?? 0)"
        
        switch fecha.month {
        case 1:
            fechaString = "01-\(fechaString) 00:00:00"
        case 2:
            fechaString = "02-\(fechaString) 00:00:00"
        case 3:
            fechaString = "03-\(fechaString) 00:00:00"
        case 4:
            fechaString = "04-\(fechaString) 00:00:00"
        case 5:
            fechaString = "05-\(fechaString) 00:00:00"
        case 6:
            fechaString = "06-\(fechaString) 00:00:00"
        case 7:
            fechaString = "07-\(fechaString) 00:00:00"
        case 8:
            fechaString = "08-\(fechaString) 00:00:00"
        case 9:
            fechaString = "09-\(fechaString) 00:00:00"
        case 10:
            fechaString = "10-\(fechaString) 00:00:00"
        case 11:
            fechaString = "11-\(fechaString) 00:00:00"
        case 12:
            fechaString = "12-\(fechaString) 00:00:00"
        default:
            fechaString = ""
        }
        
        fechaString = "\(fecha.year ?? 0)-\(fechaString)"
        
        return fechaString
    }

    //MARK: - textfield delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(campoDireccion.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -100
            })
        }else if(campoCodigoPostal.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -150
            })
        }else if(campoEstado.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -200
            })
        }else if(campoCiudad.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -250
            })
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        textField.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        textField.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(campoNombre.isFirstResponder){
            
            campoApellido.becomeFirstResponder()
        }else if(campoApellido.isFirstResponder){
            
            campoFechaNacimiento.becomeFirstResponder()
        }else if(campoFechaNacimiento.isFirstResponder){
            
            campoDireccion.becomeFirstResponder()
        }else if(campoDireccion.isFirstResponder){
            
            campoCodigoPostal.becomeFirstResponder()
        }else if(campoCodigoPostal.isFirstResponder){
            
            campoEstado.becomeFirstResponder()
        }else if(campoEstado.isFirstResponder){
            
            campoCiudad.becomeFirstResponder()
        }else if(campoCiudad.isFirstResponder){
            
            campoCiudad.resignFirstResponder()
            
            acomodarViewOrigen()
        }
        return true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if(campoNombre.isFirstResponder){
            
            campoNombre.resignFirstResponder()
        }else if(campoApellido.isFirstResponder){
            
            campoApellido.resignFirstResponder()
        }else if(campoFechaNacimiento.isFirstResponder){
            
            campoFechaNacimiento.resignFirstResponder()
        }else if(campoDireccion.isFirstResponder){
            
            campoDireccion.resignFirstResponder()
        }else if(campoCodigoPostal.isFirstResponder){
            
            campoCodigoPostal.resignFirstResponder()
        }else if(campoEstado.isFirstResponder){
            
            campoEstado.resignFirstResponder()
        }else if(campoCiudad.isFirstResponder){
            
            campoCiudad.resignFirstResponder()
        }
        
        acomodarViewOrigen()
    }
    
    func acomodarViewOrigen(){
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.view.frame.origin.y = 0
        })
    }
    
    @IBAction func siguiente(_ sender: Any) {
        
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let fomratoValoracionP2VC = storyboard.instantiateViewController(withIdentifier: "fomratoValoracionP2VC")
            
            self.navigationController?.pushViewController(fomratoValoracionP2VC, animated: true)
            
        }
    }
    
    //MARK: - navegacion
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if campoNombre.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || campoApellido.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || campoFechaNacimiento.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || campoDireccion.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || campoCodigoPostal.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || campoEstado.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || campoCiudad.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            
            if campoNombre.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                campoNombre.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            }else if campoApellido.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                campoApellido.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            }else if campoFechaNacimiento.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                campoFechaNacimiento.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            }else if campoDireccion.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                campoDireccion.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            }else if campoCodigoPostal.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                campoCodigoPostal.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            }else if campoEstado.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                campoEstado.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            }else if campoCiudad.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                campoCiudad.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 0.8)
            }
            
            return false
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let fechaFormateada = obtenerFechaFormateada(fechaDatePicker: datePicker)
        
        let respuestas = "id=\(defaults.integer(forKey: "id"))&nom=\(campoNombre.text ?? "")&ape=\(campoApellido.text ?? "")&fn=\(fechaFormateada)&dire=\(campoDireccion.text ?? "")&cp=\(campoCodigoPostal.text ?? "")&est=\(campoEstado.text ?? "")&ciu=\(campoCiudad.text ?? "")"
        
        let destino = segue.destination as! FomratoValoracionP2ViewController
        
        destino.respuestas = respuestas
    }
}
