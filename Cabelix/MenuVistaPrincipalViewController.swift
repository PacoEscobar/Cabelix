//
//  MenuVistaPrincipalViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/6/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class MenuVistaPrincipalViewController: UIViewController {

    
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var menuConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func cerrarMenu(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func disolverEnNegro(){
        
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
    }
    
    func ocultarMenu(){
        
        menuConstraint.constant = self.view.frame.size.width
    }
    
    func mostrarMenu(){
        
        menuConstraint.constant = 174
    }
}
