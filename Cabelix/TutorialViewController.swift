//
//  TutorialViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 02/11/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate {

    let defaults = UserDefaults.standard
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var empezarButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararImagenes()
    }
    
    func prepararImagenes(){
                
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        let imagen1 = UIImageView(frame: CGRect(x: 0, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height-50))
        let imagen2 = UIImageView(frame: CGRect(x: scrollView.frame.size.width, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height-50))
        let imagen3 = UIImageView(frame: CGRect(x: scrollView.frame.size.width*2, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height-50))
        let imagen4 = UIImageView(frame: CGRect(x: scrollView.frame.size.width*3, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height-50))
                
        imagen1.image = UIImage(named: "tutorial_1")
        imagen2.image = UIImage(named: "tutorial_2")
        imagen3.image = UIImage(named: "tutorial_3")
        imagen4.image = UIImage(named: "tutorial_4")
        
        imagen1.contentMode = .scaleAspectFit
        imagen2.contentMode = .scaleAspectFit
        imagen3.contentMode = .scaleAspectFit
        imagen4.contentMode = .scaleAspectFit
        
        scrollView.addSubview(imagen1)
        scrollView.addSubview(imagen2)
        scrollView.addSubview(imagen3)
        scrollView.addSubview(imagen4)
        
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width*4, height: 1.0)
        
        scrollView.delegate = self
        
        pageControl.numberOfPages = 4
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        
        pageControl.currentPage = Int(pageNumber)
        
        if pageNumber == 3{
            
            pageControl.isHidden = true
            empezarButton.isHidden = false
        }else{
            
            pageControl.isHidden = false
            empezarButton.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination.isKind(of: VistaCargaPrinipalViewController.self){
            
            defaults.setValue(1, forKey: "tutorial")
        }
    }

}
