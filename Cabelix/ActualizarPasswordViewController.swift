//
//  ActualizarPasswordViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 11/30/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class ActualizarPasswordViewController: UIViewController {

    let defaults = UserDefaults.standard
    
    @IBOutlet weak var campoPass: UITextField!
    @IBOutlet weak var notificacionListo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func actualizar(_ sender: Any) {
        
        if validarCampos() {
            
            let encriptador = Sha256ViewController()
            
            let pass256 = encriptador.sha256(str: campoPass.text!)
            
            let url = URL(string: "http://52.43.224.214/api/panel/actualizar/pass")
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = "id=\(defaults.integer(forKey: "id"))&cs=\(pass256)".data(using: .utf8)
            
            let task = URLSession.shared.dataTask(with: request){ data, response, error in
                
                if data != nil{
                    
                    let respuesta = String(data: data!, encoding: .utf8)
                    
                    if respuesta == "listo"{
                        
                        DispatchQueue.main.async {
                            
                            self.notificacionListo.isHidden = false
                        }
                    }
                    
                }
            }
            
            task.resume()
        }
    }
    func validarCampos() -> Bool{
        
        if campoPass.text != ""{
            
            return true
        }else{
            
            campoPass.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            return false
        }
    }
    
}
