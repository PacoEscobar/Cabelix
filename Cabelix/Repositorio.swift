//
//  Repositorio.swift
//  Cabelix
//
//  Created by Francisco Escobar on 23/10/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class Repositorio{
    
    let defaults = UserDefaults.standard
    var mensajesJson:[[String:Any]] = [[String:Any]]()
    var mensajes:[Mensaje] = [Mensaje]()
    
    func obtenerMensaje(listaMensajesTableView: UITableView){
        
        let url = URL(string: "http://52.43.224.214/api/panel/mensajesApp")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = "id=\(defaults.integer(forKey: "id"))".data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
           
            if data != nil{
                
                
                DispatchQueue.main.async{
                    do{
                        self.mensajesJson = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [[String: Any]]
                                                
                        for mensajeJson in self.mensajesJson{
                            
                            let mensaje = Mensaje()
                            mensaje.id = mensajeJson["mensajeId"] as! Int
                            mensaje.mensaje = mensajeJson["mensaje"] as! String
                            mensaje.tieneImagen = mensajeJson["image"] as! Int
                            
                            if mensaje.tieneImagen == 1{
                                
                                mensaje.nombreImagen = mensajeJson["nombreimagen"] as! String
                                
                                let imagenURL = "http://52.43.224.214/images/promocionales/\(mensaje.nombreImagen)"

                                self.descargarImagen(from: imagenURL, completionHandler: {data in
                                    
                                    mensaje.imagen = UIImage(data: data!)!
                                    
                                })
                            }
                            
                            self.mensajes.append(mensaje)
                        }
                                                
                        let mensajesUserInfo: [String: Any] = ["mensajes":self.mensajes]
                        NotificationCenter.default.post(name:.mensajes, object: nil, userInfo: mensajesUserInfo)
                        
                    }catch{}
                }
            }
        }
        
        task.resume()
    }
    
    func descargarImagen(from urlString: String, completionHandler: @escaping (_ data: Data?) -> ()) {
        let session = URLSession.shared
        let url = URL(string: urlString)
            
        let dataTask = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print("Error fetching the image! 😢")
                completionHandler(nil)
            } else {
                completionHandler(data)
            }
        }
            
        dataTask.resume()
    }
}
