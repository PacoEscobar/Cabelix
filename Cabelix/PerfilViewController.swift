//
//  PerfilViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/22/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class PerfilViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var tabla: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        tabla.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celda = UITableViewCell()
        
        if indexPath.row == 0{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaNombre")!
            
            let label = celda.viewWithTag(1) as! UILabel
            
            label.text = "Nombre: \(defaults.string(forKey: "nombre") ?? "")"
        }else if indexPath.row == 1{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaApellido")!
            
            let label = celda.viewWithTag(1) as! UILabel
            
            label.text = "Apellido: \(defaults.string(forKey: "apellido") ?? "")"
        }else if indexPath.row == 2{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaCorreo")!
            
            let label = celda.viewWithTag(1) as! UILabel
            
            label.text = "Correo: \(defaults.string(forKey: "correo") ?? "")"
        }else if indexPath.row == 3{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaPass")!
        }
        
        return celda
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 105
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let actualizarNombreVC = storyboard.instantiateViewController(withIdentifier: "actualizarNombreVC")
            
            self.navigationController?.pushViewController(actualizarNombreVC, animated: true)
        }else if indexPath.row == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let actualizarApellidoVC = storyboard.instantiateViewController(withIdentifier: "actualizarApellidoVC")
            
            self.navigationController?.pushViewController(actualizarApellidoVC, animated: true)
        }else if indexPath.row == 2{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let actualizarCorreoVC = storyboard.instantiateViewController(withIdentifier: "actualizarCorreoVC")
            
            //self.navigationController?.pushViewController(actualizarCorreoVC, animated: true)
        }else if indexPath.row == 3{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let confirmarPasswordVC = storyboard.instantiateViewController(withIdentifier: "confirmarPasswordVC")
            
            self.navigationController?.pushViewController(confirmarPasswordVC, animated: true)
        }
    }
    
}
