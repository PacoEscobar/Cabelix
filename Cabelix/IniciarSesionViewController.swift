//
//  IniciarSesionViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/1/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit
import CommonCrypto

class IniciarSesionViewController: UIViewController {

    @IBOutlet weak var campoCorreo: UITextField!
    @IBOutlet weak var campoPassword: UITextField!
    
    var respuestaInicioSesion: [String: Any] = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let uitapgestureCerrarTextField = UITapGestureRecognizer(target: self, action: #selector(cerrarTextField))
        
        self.view.addGestureRecognizer(uitapgestureCerrarTextField)

    }
    
    @IBAction func iniciarSesion(_ sender: Any) {
        
        let contraseña = sha256(str: campoPassword.text!)
        
        let url = URL(string: "http://52.43.224.214/api/iniciosesion")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = "co=\(campoCorreo.text ?? "-")&cs=\(contraseña)".data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            
            let s = String(data: data!, encoding: .utf8)
            
            print("llega \(String(describing: s))")
                        
            if data != nil{
                
                do{
                    
                    self.respuestaInicioSesion = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: Any]
                    
                    if(self.respuestaInicioSesion["status"] as! Int == 1){
                        
                        DispatchQueue.main.async {
                                                        
                            let defaults = UserDefaults.standard
                            defaults.set(self.respuestaInicioSesion["ID"], forKey: "id")
                            defaults.set(self.respuestaInicioSesion["nombre"], forKey: "nombre")
                            defaults.set(self.respuestaInicioSesion["apellido"], forKey: "apellido")
                            defaults.set(self.campoCorreo.text, forKey: "correo")
                            
                            defaults.set(self.respuestaInicioSesion["tipo_cuenta"], forKey: "tipo_cuenta")
                            
                            defaults.set(self.respuestaInicioSesion["codigo_afiliado"], forKey: "codigo_afiliado")
                            defaults.set(self.respuestaInicioSesion["fecha_ultima_valoracion"], forKey: "fecha_ultima_valoracion")
                            
                            
                            defaults.set(self.respuestaInicioSesion["mensaje"], forKey: "mensaje_dia")
                            
                            defaults.set(self.respuestaInicioSesion["referidos"], forKey: "referidos")
                            
                            defaults.set(self.respuestaInicioSesion["siguienteBono"], forKey: "siguienteBono")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let navigationControllerPrincipal = storyboard.instantiateViewController(withIdentifier: "tabBarControllerPrincipal")
                            
                            navigationControllerPrincipal.modalPresentationStyle = .fullScreen
                            
                            self.present(navigationControllerPrincipal, animated: true, completion: nil)
                            
                        }
                    }else if(self.respuestaInicioSesion["status"] as! Int == 2) {
                        //correo mal
                        
                        DispatchQueue.main.async {
                            
                            let alertControler = UIAlertController(title: "Contraseña incorrecta", message: "Por favor revisa tu contraseña", preferredStyle: .alert)
                            
                            let alertAction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
                            
                            alertControler.addAction(alertAction)
                            
                            self.present(alertControler, animated: true, completion: nil)
                        }
                        
                        
                    }else if(self.respuestaInicioSesion["status"] as! Int == 3) {
                        // usuario no existe
                        
                        DispatchQueue.main.async {
                            
                            let alertControler = UIAlertController(title: nil, message: "Aún no te has registrado, vamos a registrarte ahora", preferredStyle: .alert)
                            
                            let alertAction = UIAlertAction(title: "¡Vamos!", style: .default, handler: {_ in
                                
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let registrarseVC = storyboard.instantiateViewController(withIdentifier: "registrarseVC") as! RegistrarseViewController
                                
                                registrarseVC.correoString = self.campoCorreo.text ?? ""
                                
                                self.navigationController?.pushViewController(registrarseVC, animated: true)
                            })
                            
                            alertControler.addAction(alertAction)
                            
                            self.present(alertControler, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                }catch{}
            }
        }
        
        task.resume()
    }
    
    
    func sha256(str: String) -> String {
        
        if let strData = str.data(using: String.Encoding.utf8) {
            /// #define CC_SHA256_DIGEST_LENGTH     32
            /// Creates an array of unsigned 8 bit integers that contains 32 zeros
            var digest = [UInt8](repeating: 0, count:Int(CC_SHA256_DIGEST_LENGTH))
            
            /// CC_SHA256 performs digest calculation and places the result in the caller-supplied buffer for digest (md)
            /// Takes the strData referenced value (const unsigned char *d) and hashes it into a reference to the digest parameter.
            strData.withUnsafeBytes {
                // CommonCrypto
                // extern unsigned char *CC_SHA256(const void *data, CC_LONG len, unsigned char *md)  -|
                // OpenSSL                                                                             |
                // unsigned char *SHA256(const unsigned char *d, size_t n, unsigned char *md)        <-|
                CC_SHA256($0.baseAddress, UInt32(strData.count), &digest)
            }
            
            var sha256String = ""
            /// Unpack each byte in the digest array and add them to the sha256String
            for byte in digest {
                sha256String += String(format:"%02x", UInt8(byte))
            }
            
            return sha256String
        }
        return ""
    }
    
    @objc func cerrarTextField(){
        
        if campoCorreo.isFirstResponder{
            
            campoCorreo.resignFirstResponder()
        }else if campoPassword.isFirstResponder{
            
            campoPassword.resignFirstResponder()
        }
    }
}
