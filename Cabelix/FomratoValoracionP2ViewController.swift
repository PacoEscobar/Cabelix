//
//  FomratoValoracionP2ViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/10/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class FomratoValoracionP2ViewController: UIViewController, UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var campoCorreo: UITextField!
    @IBOutlet weak var campoFechaValoracion: UITextField!
    @IBOutlet weak var campoTelefonoCasa: UITextField!
    @IBOutlet weak var campoTelefonoOficina: UITextField!
    @IBOutlet weak var campoCelular: UITextField!
    @IBOutlet weak var campoProfesion: UITextField!
    @IBOutlet weak var campoEstadoCivil: UITextField!
    @IBOutlet weak var campoComoEnteroCabelix: UITextField!
    @IBOutlet weak var campoEspecifiqueMedio: UITextField!
    
    @IBOutlet weak var constraintEspecifiqueMedioBottom: NSLayoutConstraint!
    
    let defaults = UserDefaults.standard
    var datePicker: UIDatePicker = UIDatePicker()
    var enteroCabelixPicker: UIPickerView = UIPickerView()
    let opcionesEnteroCabelix = ["Facebook", "Recomendado", "Sitio web", "Otro"]
    var tecladoOrigin: CGFloat = 0.0
    var respuestas = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
        cargarDatosGuardados()
    }
    
    //MARK: - funciones
    
    func prepararUI(){

        let fechaActual = Date()
        
        constraintEspecifiqueMedioBottom.constant = 5
        
        datePicker.datePickerMode = .date
        if #available(iOS 14.0, *) {
            datePicker.preferredDatePickerStyle = .inline
        }
        campoFechaValoracion.inputView = datePicker
        campoComoEnteroCabelix.inputView = enteroCabelixPicker
        enteroCabelixPicker.delegate = self
        
        datePicker.addTarget(self, action: #selector(cambioFechaValoracion), for: .valueChanged)
        datePicker.minimumDate = fechaActual
        
        NotificationCenter.default.addObserver(self, selector: #selector(obtenerTecladoSize(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    func cargarDatosGuardados(){
        
        campoCorreo.text = defaults.string(forKey: "correo")
        campoCorreo.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        campoCorreo.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
    }
    
    @objc func obtenerTecladoSize(notification: Notification){
        
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            
            return
        }
        
        tecladoOrigin = keyboardRect.height
        
    }
    
    @objc func cambioFechaValoracion(){
        
        let fecha = Calendar.current.dateComponents([.day, .month, .year], from: datePicker.date)
        
        var fechaString = "\(fecha.day ?? 0)"
        
        switch fecha.month {
        case 1:
            fechaString = "\(fechaString) ene"
        case 2:
            fechaString = "\(fechaString) feb"
        case 3:
            fechaString = "\(fechaString) mar"
        case 4:
            fechaString = "\(fechaString) abr"
        case 5:
            fechaString = "\(fechaString) may"
        case 6:
            fechaString = "\(fechaString) jun"
        case 7:
            fechaString = "\(fechaString) jul"
        case 8:
            fechaString = "\(fechaString) ago"
        case 9:
            fechaString = "\(fechaString) sep"
        case 10:
            fechaString = "\(fechaString) oct"
        case 11:
            fechaString = "\(fechaString) nov"
        case 12:
            fechaString = "\(fechaString) dic"
        default:
            fechaString = ""
        }
        
        fechaString = "\(fechaString) \(fecha.year ?? 0)"
        
        campoFechaValoracion.text = fechaString
        
        campoFechaValoracion.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        campoFechaValoracion.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
    }
    
    func obtenerFechaFormateada(fechaDatePicker: UIDatePicker) -> String{
        
        let fecha = Calendar.current.dateComponents([.day, .month, .year], from: fechaDatePicker.date)
        
        var fechaString = "\(fecha.day ?? 0)"
        
        switch fecha.month {
        case 1:
            fechaString = "01-\(fechaString) 00:00:00"
        case 2:
            fechaString = "02-\(fechaString) 00:00:00"
        case 3:
            fechaString = "03-\(fechaString) 00:00:00"
        case 4:
            fechaString = "04-\(fechaString) 00:00:00"
        case 5:
            fechaString = "05-\(fechaString) 00:00:00"
        case 6:
            fechaString = "06-\(fechaString) 00:00:00"
        case 7:
            fechaString = "07-\(fechaString) 00:00:00"
        case 8:
            fechaString = "08-\(fechaString) 00:00:00"
        case 9:
            fechaString = "09-\(fechaString) 00:00:00"
        case 10:
            fechaString = "10-\(fechaString) 00:00:00"
        case 11:
            fechaString = "11-\(fechaString) 00:00:00"
        case 12:
            fechaString = "12-\(fechaString) 00:00:00"
        default:
            fechaString = ""
        }
        
        fechaString = "\(fecha.year ?? 0)-\(fechaString)"
        
        return fechaString
    }
    
    func camposShouldContinue () -> Bool{
        
        var continua = true
        
        if campoCorreo.text == ""{
            
            campoCorreo.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            continua = false
        }else if campoFechaValoracion.text == ""{
            
            campoFechaValoracion.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            continua = false
        }else if campoTelefonoCasa.text == ""{
            
            campoTelefonoCasa.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            continua = false
        }else if campoTelefonoOficina.text == ""{
            
            campoTelefonoOficina.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            continua = false
        }else if campoCelular.text == ""{
            
            campoCelular.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            continua = false
        }else if campoProfesion.text == ""{
            
            campoProfesion.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            continua = false
        }else if campoEstadoCivil.text == ""{
            
            campoEstadoCivil.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            continua = false
        }else if campoComoEnteroCabelix.text == ""{
            
            campoComoEnteroCabelix.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            continua = false
        }else if campoEspecifiqueMedio.text == ""{
            
            if enteroCabelixPicker.selectedRow(inComponent: 0) == 3{
                
                campoEspecifiqueMedio.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                continua = false
            }
        }
        
        return continua
    }
    
    //MARK: - delegados
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if(campoFechaValoracion.isFirstResponder){
            
            campoFechaValoracion.resignFirstResponder()
        }else if(campoCorreo.isFirstResponder){
            
            campoCorreo.resignFirstResponder()
        }else if(campoTelefonoCasa.isFirstResponder){
            
            campoTelefonoCasa.resignFirstResponder()
        }else if(campoTelefonoOficina.isFirstResponder){
            
            campoTelefonoOficina.resignFirstResponder()
        }else if(campoCelular.isFirstResponder){
            
            campoCelular.resignFirstResponder()
        }else if(campoProfesion.isFirstResponder){
            
            campoProfesion.resignFirstResponder()
        }else if(campoEstadoCivil.isFirstResponder){
            
            campoEstadoCivil.resignFirstResponder()
        }else if(campoComoEnteroCabelix.isFirstResponder){

            campoComoEnteroCabelix.resignFirstResponder()
        }else if(campoEspecifiqueMedio.isFirstResponder){
            
            campoEspecifiqueMedio.resignFirstResponder()
        }
        
        acomodarViewOrigen()
    }
    
    // MARK: - picker view
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return opcionesEnteroCabelix.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return opcionesEnteroCabelix[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        campoComoEnteroCabelix.text = opcionesEnteroCabelix[row]
        
        campoComoEnteroCabelix.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        campoComoEnteroCabelix.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
        
        if row == 3{
            
            campoEspecifiqueMedio.isHidden = false
        }else{
            
            campoEspecifiqueMedio.isHidden = true
        }
    }
    
    //MARK:- text field
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        if textField == campoComoEnteroCabelix && enteroCabelixPicker.selectedRow(inComponent: 0) == 0{
            
            textField.text = "Facebook"
        }
        
        if(campoCelular.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -100
            })
        }else if(campoCorreo.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -150
            })
        }else if(campoProfesion.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -200
            })
        }else if(campoEstadoCivil.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -200
            })
        }else if(campoComoEnteroCabelix.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -200
            })
        }else if(campoEspecifiqueMedio.isFirstResponder){
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -250
            })
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        textField.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        textField.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(campoTelefonoCasa.isFirstResponder){
            
            campoTelefonoOficina.becomeFirstResponder()
        }else if(campoTelefonoOficina.isFirstResponder){
            
            campoCelular.becomeFirstResponder()
        }else if(campoCelular.isFirstResponder){
            
            campoCorreo.becomeFirstResponder()
        }else if(campoCorreo.isFirstResponder){
            
            campoProfesion.becomeFirstResponder()
        }else if(campoProfesion.isFirstResponder){
            
            campoEstadoCivil.becomeFirstResponder()
        }else if(campoEstadoCivil.isFirstResponder){
            
            campoComoEnteroCabelix.becomeFirstResponder()
        }else if(campoComoEnteroCabelix.isFirstResponder){
            
            if campoComoEnteroCabelix.text == "Otro"{
            
                campoEspecifiqueMedio.becomeFirstResponder()
            }else{
                
                campoEspecifiqueMedio.resignFirstResponder()
                
                acomodarViewOrigen()
            }
        }else if(campoEspecifiqueMedio.isFirstResponder){
            
            campoEspecifiqueMedio.resignFirstResponder()
            
            acomodarViewOrigen()
        }
        return true
    }
    
    func acomodarViewOrigen(){
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.view.frame.origin.y = 0
        })
    }
    
    //MARK: - navegacion
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        let fechaFormateada = obtenerFechaFormateada(fechaDatePicker: datePicker)
        let enteroCabelixSelectedRow = enteroCabelixPicker.selectedRow(inComponent: 0)
        
        respuestas = "\(respuestas)&fv=\(fechaFormateada)&tc=\(campoTelefonoCasa.text ?? "")&to=\(campoTelefonoOficina.text ?? "")&cel=\(campoCelular.text ?? "")&cor=\(campoCorreo.text ?? "")&pro=\(campoProfesion.text ?? "")&ec=\(campoEstadoCivil.text ?? "")&ce=\(enteroCabelixSelectedRow)&ceo=\(campoEspecifiqueMedio.text ?? "")"
        
        
        let destino = segue.destination as! FomratoValoracionP3ViewController
        
        destino.respuestas = respuestas
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if camposShouldContinue(){
            
            return true
        }
        
        return false
    }
    
}
