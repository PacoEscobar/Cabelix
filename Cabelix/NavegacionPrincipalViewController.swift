//
//  NavegacionPrincipalViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/2/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class NavegacionPrincipalViewController: UINavigationController {

    let defaults = UserDefaults.standard
    let transicion = MenuHamburguesaAnimator()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
}

/*extension NavegacionPrincipalViewController:UIViewControllerTransitioningDelegate{
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let menuVC = segue.destination as! MenuVistaPrincipalViewController
        
        menuVC.transitioningDelegate = self
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        transicion.presenting = true
        
        return transicion
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        transicion.presenting = false
        
        return transicion
    }
}*/
