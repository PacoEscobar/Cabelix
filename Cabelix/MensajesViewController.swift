//
//  MensajesViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 10/5/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class MensajesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let defaults = UserDefaults.standard
    
    var mensajes:[Mensaje] = [Mensaje]()
    
    private let notificationCenter = NotificationCenter()
    
    @IBOutlet weak var listaMensajesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
        
        let repositorio = Repositorio()
        repositorio.obtenerMensaje(listaMensajesTableView: listaMensajesTableView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(llegoMensajes(_:)), name: .mensajes, object: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mensajes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaMensaje")
        
        let mensaje = celda?.viewWithTag(1) as! UILabel
        
        //mensaje.text = "\(mensajes[indexPath.row]["mensaje"] ?? "")"
        
        mensaje.text = mensajes[indexPath.row].mensaje
        
        return celda!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mensajeVC = storyboard.instantiateViewController(withIdentifier: "mensajeVC") as! MensajeViewController
        
        mensajeVC.mensaje = mensajes[indexPath.row]

        self.navigationController?.pushViewController(mensajeVC, animated: true)
    }
    
    @objc func llegoMensajes(_ notification:Notification){

        let mensajesTopLevel = notification.userInfo as! [String:Any]
        
        mensajes = mensajesTopLevel["mensajes"] as! [Mensaje]

        if mensajes.count > 0{
            
            listaMensajesTableView.reloadData()
            listaMensajesTableView.isHidden = false
        }
    }
    
}
