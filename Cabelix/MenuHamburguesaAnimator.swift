//
//  MenuHamburguesaAnimator.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/6/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class MenuHamburguesaAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 0.3
    var presenting = true
    var originalFrame = CGRect.zero
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let contanierView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: .to)
        let toView = toViewController?.view
        let fromViewController = transitionContext.viewController(forKey: .from)
        let fromView = fromViewController?.view
        
        //contanierView.addSubview(fromView!)
        
        
        if presenting{
            
            toView!.transform = CGAffineTransform(translationX: toView!.frame.size.width, y: 0)
            
            let fondoNegro = UIView(frame: CGRect(x: 0, y: 0, width: (fromView?.frame.size.width)!, height: (fromView?.frame.size.height)!))
            fondoNegro.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            fondoNegro.tag = 9999
            fromView?.addSubview(fondoNegro)
            fromView?.bringSubviewToFront(fondoNegro)
            
            contanierView.addSubview(toView!)
            contanierView.bringSubviewToFront(fromView!)
            
            UIView.animate(withDuration: 0.1, animations: {
                
                fondoNegro.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                
            }, completion: { _ in
                
                UIView.animate(withDuration: self.duration, animations: {
                    
                    toView!.transform = CGAffineTransform(translationX: 0, y: 0)
                }, completion: { _ in
                    
                    transitionContext.completeTransition(true)
                })
            })
        }else{
            
            let fondoNegro = toView?.viewWithTag(9999)
            
            contanierView.addSubview(toView!)
            contanierView.bringSubviewToFront(fromView!)
            
            UIView.animate(withDuration: 0.1, animations: {
                
                fondoNegro?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            }, completion: { _ in
                fondoNegro?.removeFromSuperview()
                UIView.animate(withDuration: self.duration, animations: {
                    
                    fromView!.transform = CGAffineTransform(translationX: fromView!.frame.size.width, y: 0)
                    
                    let toViewA = transitionContext.view(forKey: .from)
                    
                    print("to a \(String(describing: toViewController))")
                    
                   // print("from view \(String(describing: fromView)) to view \(String(describing: toView))")
                    
                })
            })
        }
    }

}
