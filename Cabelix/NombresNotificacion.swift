//
//  NombresNotificacion.swift
//  Cabelix
//
//  Created by Francisco Escobar on 23/10/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation

public extension Notification.Name{
    
    static let mensajes = Notification.Name("mensajes")
    
}
