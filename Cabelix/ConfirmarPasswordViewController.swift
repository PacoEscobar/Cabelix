//
//  ConfirmarPasswordViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 11/30/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class ConfirmarPasswordViewController: UIViewController {

    let defaults = UserDefaults.standard
    
    @IBOutlet weak var campoPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func enviar(_ sender: Any) {

        let encriptador = Sha256ViewController()
        let passwordEncriptado = encriptador.sha256(str: campoPassword.text!)
        
        let url = URL(string: "http://52.43.224.214/api/panel/comprobador/pass")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = "id=\(defaults.integer(forKey: "id"))&cs=\(passwordEncriptado)".data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            
            if data != nil{
                
                let respuesta = String(data: data!, encoding: .utf8)
                let respuestaInteger = Int(respuesta!)

                if respuestaInteger == 1{
                    
                    DispatchQueue.main.async {
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let actualizarPassVC = storyboard.instantiateViewController(withIdentifier: "actualizarPassVC")
                        
                        self.navigationController?.pushViewController(actualizarPassVC, animated: true)
                    }
                }
            }
        }
        
        task.resume()
    }
    
}
