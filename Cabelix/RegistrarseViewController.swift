//
//  RegistrarseViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/1/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit
import CommonCrypto

class RegistrarseViewController: UIViewController, UITextFieldDelegate {

    var correoString = ""
    
    @IBOutlet weak var campoNombre: UITextField!
    @IBOutlet weak var campoApellido: UITextField!
    @IBOutlet weak var campoCorreo: UITextField!
    @IBOutlet weak var campoContra: UITextField!
    
    @IBOutlet weak var botonRegistrate: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var respuestaRegistro: [String:Any] = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if correoString != "" {
            
            campoCorreo.text = correoString
        }
        
    }

    
    @IBAction func enviar(_ sender: Any) {
        
        botonRegistrate.isHidden = true
        activityIndicator.isHidden = false
        
        let contraseña = sha256(str: campoContra.text!)
        
        let url = URL(string: "http://52.43.224.214/api/registro")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = "nm=\(campoNombre.text ?? "-")&ap=\(campoApellido.text ?? "-")&cor=\(campoCorreo.text ?? "-")&cs=\(contraseña)".data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            
            if data != nil{
                
                do{
                    
                    self.respuestaRegistro = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: Any]
                    
                    
                    DispatchQueue.main.async {
                        let defaults = UserDefaults.standard
                        defaults.set(self.respuestaRegistro["ID"], forKey: "id")
                        defaults.set(self.campoNombre.text, forKey: "nombre")
                        defaults.set(self.campoApellido.text, forKey: "apellido")
                        defaults.set(self.campoCorreo.text, forKey: "correo")
                        defaults.set(1, forKey: "tipo_cuenta")
                        defaults.set("0", forKey: "fecha_ultima_valoracion")
                                                
                        defaults.set(self.respuestaRegistro["codigo_afiliado"], forKey: "codigo_afiliado")
                        defaults.set(self.respuestaRegistro["fecha_ultima_valoracion"], forKey: "fecha_ultima_valoracion")
                        
                        
                        defaults.set(self.respuestaRegistro["mensaje"], forKey: "mensaje_dia")
                        
                        defaults.set(self.respuestaRegistro["referidos"], forKey: "referidos")
                        
                        defaults.set(self.respuestaRegistro["siguienteBono"], forKey: "siguienteBono")
                        
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navigationControllerPrincipal = storyboard.instantiateViewController(withIdentifier: "tabBarControllerPrincipal")
                        
                        navigationControllerPrincipal.modalPresentationStyle = .fullScreen
                        
                        self.present(navigationControllerPrincipal, animated: true, completion: nil)
                    }
                    
                }catch{}
            }
        }
        
        task.resume()
    }
    
    
    func sha256(str: String) -> String {
        
        if let strData = str.data(using: String.Encoding.utf8) {
            /// #define CC_SHA256_DIGEST_LENGTH     32
            /// Creates an array of unsigned 8 bit integers that contains 32 zeros
            var digest = [UInt8](repeating: 0, count:Int(CC_SHA256_DIGEST_LENGTH))
            
            /// CC_SHA256 performs digest calculation and places the result in the caller-supplied buffer for digest (md)
            /// Takes the strData referenced value (const unsigned char *d) and hashes it into a reference to the digest parameter.
            strData.withUnsafeBytes {
                // CommonCrypto
                // extern unsigned char *CC_SHA256(const void *data, CC_LONG len, unsigned char *md)  -|
                // OpenSSL                                                                             |
                // unsigned char *SHA256(const unsigned char *d, size_t n, unsigned char *md)        <-|
                CC_SHA256($0.baseAddress, UInt32(strData.count), &digest)
            }
            
            var sha256String = ""
            /// Unpack each byte in the digest array and add them to the sha256String
            for byte in digest {
                sha256String += String(format:"%02x", UInt8(byte))
            }
            
            return sha256String
        }
        return ""
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == campoCorreo{
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.view.frame = CGRect(x: self.view.frame.origin.x,y: -50,width: self.view.frame.size.width,height: self.view.frame.size.height)
            })
        }
        
        if textField == campoContra{
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.view.frame = CGRect(x: self.view.frame.origin.x,y: -100,width: self.view.frame.size.width,height: self.view.frame.size.height)
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == campoContra{
            
            campoContra.resignFirstResponder()
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.view.frame = CGRect(x: self.view.frame.origin.x,y: 0,width: self.view.frame.size.width,height: self.view.frame.size.height)
            })
        }
        
        return true
    }
}
