//
//  FomratoValoracionP4ViewControllerViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 10/8/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class FomratoValoracionP4ViewControllerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate, UITextFieldDelegate {

    let defaults = UserDefaults.standard
    
    var respuestas = ""
    var tratoPerdida = 0
    
    @IBOutlet weak var campoObservoResultados: UITextField!
    @IBOutlet weak var campoFrecuenciaLavaCabello: UITextField!
    @IBOutlet weak var campoOtroFrecuenciaLava: UITextField!
    @IBOutlet weak var campoCambiosCueroCabelludo: UITextField!
    @IBOutlet weak var campoCuales: UITextField!
    @IBOutlet weak var campoHaceCuanto: UITextField!
    @IBOutlet weak var campoComezonCueroCabelludo: UITextField!
    @IBOutlet weak var campoRascarse: UITextField!
    @IBOutlet weak var campoCicatrizacion: UITextField!
    @IBOutlet weak var campoOtroCicatrizacion: UITextField!
    @IBOutlet weak var campoConsultor: UITextField!
    
    @IBOutlet weak var constraintPresentadoComezon: NSLayoutConstraint!
    @IBOutlet weak var constraintTipoCicatrizacion: NSLayoutConstraint!
    @IBOutlet weak var constraintConsultor: NSLayoutConstraint!
    @IBOutlet weak var constraintNotadoCambios: NSLayoutConstraint!
    
    
    var observoResultadosPickerView: UIPickerView = UIPickerView()
    let opcionesObservoResultados = ["Sí", "No", "Poco", "Mucho"]
    
    var frecuenciaLavaCabelloPickerView: UIPickerView = UIPickerView()
    let opcionesFrecuenciaLavaCabello = ["Diario", "Cada 2 días", "Otro"]
    
    var notoCambiosPickerView: UIPickerView = UIPickerView()
    
    var comezonCueroCabelludoPickerView: UIPickerView = UIPickerView()
    let opcionesSiNo = ["Sí", "No"]
    
    var rascarseCueroCabelludoPickerView: UIPickerView = UIPickerView()
    
    var tipoCicatrizacionPickerView: UIPickerView = UIPickerView()
    let opcionesTipoCicatrizacion = ["Normal", "Queloide", "Otra"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        observoResultadosPickerView.delegate = self
        campoObservoResultados.inputView = observoResultadosPickerView
        
        frecuenciaLavaCabelloPickerView.delegate = self
        campoFrecuenciaLavaCabello.inputView = frecuenciaLavaCabelloPickerView
        
        notoCambiosPickerView.delegate = self
        campoCambiosCueroCabelludo.inputView = notoCambiosPickerView
        
        comezonCueroCabelludoPickerView.delegate = self
        campoComezonCueroCabelludo.inputView = comezonCueroCabelludoPickerView
        
        rascarseCueroCabelludoPickerView.delegate = self
        campoRascarse.inputView = rascarseCueroCabelludoPickerView
        
        tipoCicatrizacionPickerView.delegate = self
        campoCicatrizacion.inputView = tipoCicatrizacionPickerView
        
        if tratoPerdida == 0{
            
            campoObservoResultados.isHidden = false
            campoFrecuenciaLavaCabello.isHidden = false
            campoCambiosCueroCabelludo.isHidden = false
            
            constraintPresentadoComezon.constant = 149
            //constraintPresentadoComezon.constant = 215
            constraintNotadoCambios.constant = 18
        }else{
        
            constraintPresentadoComezon.constant = 8
        }
        
        constraintTipoCicatrizacion.constant = 18
        constraintConsultor.constant = 18
    }

    
    // MARK: - picker view
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
       if pickerView == observoResultadosPickerView{
            return opcionesObservoResultados.count
        }else if pickerView == frecuenciaLavaCabelloPickerView{
            return opcionesFrecuenciaLavaCabello.count
       }else if pickerView == tipoCicatrizacionPickerView{
            return opcionesTipoCicatrizacion.count
       }else{
            return opcionesSiNo.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
       if pickerView == observoResultadosPickerView{
            return opcionesObservoResultados[row]
        }else if pickerView == frecuenciaLavaCabelloPickerView{
            return opcionesFrecuenciaLavaCabello[row]
        }else if pickerView == tipoCicatrizacionPickerView{
            return opcionesTipoCicatrizacion[row]
        }else{
            return opcionesSiNo[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if pickerView == observoResultadosPickerView{
            
            campoObservoResultados.text =  opcionesObservoResultados[row]
            
            campoObservoResultados.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoObservoResultados.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
        }else if pickerView == frecuenciaLavaCabelloPickerView{
            
            campoFrecuenciaLavaCabello.text =  opcionesFrecuenciaLavaCabello[row]
            
            campoFrecuenciaLavaCabello.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoFrecuenciaLavaCabello.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if frecuenciaLavaCabelloPickerView.selectedRow(inComponent: 0) == 2{
                
                campoOtroFrecuenciaLava.isHidden = false
                
                constraintPresentadoComezon.constant = 197
                constraintNotadoCambios.constant = 66
            }else{
                
                campoOtroFrecuenciaLava.isHidden = true
                
                constraintPresentadoComezon.constant = 149
                constraintNotadoCambios.constant = 18
            }
            
        }else if pickerView == notoCambiosPickerView{
            
            campoCambiosCueroCabelludo.text =  opcionesSiNo[row]
            
            campoCambiosCueroCabelludo.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoCambiosCueroCabelludo.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if notoCambiosPickerView.selectedRow(inComponent: 0) == 0{
                
                campoCuales.isHidden = false
                campoHaceCuanto.isHidden = false
                
                //constraintPresentadoComezon.constant = 248
                
                if frecuenciaLavaCabelloPickerView.selectedRow(inComponent: 0) == 2{
                    
                    constraintPresentadoComezon.constant = 300
                }else{
                    constraintPresentadoComezon.constant = 248
                }
            }else{
                
                campoCuales.isHidden = true
                campoHaceCuanto.isHidden = true
                
                if tratoPerdida == 0{
                
                    constraintPresentadoComezon.constant = 215
                }else{
                    
                    constraintPresentadoComezon.constant = 8
                }
            }
            
        }else if pickerView == comezonCueroCabelludoPickerView{
            
            campoComezonCueroCabelludo.text =  opcionesSiNo[row]
            
            campoComezonCueroCabelludo.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoComezonCueroCabelludo.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if comezonCueroCabelludoPickerView.selectedRow(inComponent: 0) == 0{
                
                campoRascarse.isHidden = false
                
                constraintTipoCicatrizacion.constant = 66
            }else{
                
                campoRascarse.isHidden = true
                
                constraintTipoCicatrizacion.constant = 18
            }
        }else if pickerView == rascarseCueroCabelludoPickerView{
            
            campoRascarse.text = opcionesSiNo[row]
            
            campoRascarse.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoRascarse.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
        }else if pickerView == tipoCicatrizacionPickerView{
            
            campoCicatrizacion.text = opcionesTipoCicatrizacion[row]
            campoCicatrizacion.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoCicatrizacion.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if row == 2{
                
                campoOtroCicatrizacion.isHidden = false
                
                constraintConsultor.constant = 66
            }else{
                
                campoOtroCicatrizacion.isHidden = true
                
                constraintConsultor.constant = 18
            }
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        campoObservoResultados.resignFirstResponder()
        campoFrecuenciaLavaCabello.resignFirstResponder()
        campoCambiosCueroCabelludo.resignFirstResponder()
        campoCuales.resignFirstResponder()
        campoHaceCuanto.resignFirstResponder()
        campoComezonCueroCabelludo.resignFirstResponder()
        campoRascarse.resignFirstResponder()
    }
    
    // MARK: - textfield
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        textField.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        textField.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
        
        return true
    }
    
    // MARK: - metodos
    
    @IBAction func finalizar(_ sender: Any) {
        
        respuestas = "\(respuestas)&or=\(observoResultadosPickerView.selectedRow(inComponent: 0))&fl=\(frecuenciaLavaCabelloPickerView.selectedRow(inComponent: 0))&flo=\(campoOtroFrecuenciaLava.text ?? "")&nc=\(notoCambiosPickerView.selectedRow(inComponent: 0))&ncc=\(campoCuales.text ?? "")&nchc=\(campoHaceCuanto.text ?? "")&ccc=\(comezonCueroCabelludoPickerView.selectedRow(inComponent: 0))&rcc=\(rascarseCueroCabelludoPickerView.selectedRow(inComponent: 0))&tic=\(tipoCicatrizacionPickerView.selectedRow(inComponent: 0))&otic=\(campoOtroCicatrizacion.text ?? "")&consul=\(campoConsultor.text ?? "")&cede=1"
        
        if formatoShouldContinue(){
            
            let date = Date()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let fecha_valoracion = dateFormatter.string(from: date)
            
            defaults.set(fecha_valoracion, forKey: "fecha_ultima_valoracion")
            
            let url = URL(string: "http://52.43.224.214/api/guardarvaloracion")
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = respuestas.data(using: .utf8)

            let task = URLSession.shared.dataTask(with: request){ data, response, error in

                if data != nil{

                    let respuesta = String(data: data!, encoding: .utf8)

                    print("Finalizar valoración: \(String(describing: respuesta))")

                    if respuesta == "listo"{

                        DispatchQueue.main.async {

                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let terminoValoracionVC = storyboard.instantiateViewController(withIdentifier: "terminoValoracionVC")

                            terminoValoracionVC.modalPresentationStyle = .fullScreen

                            self.present(terminoValoracionVC, animated: true, completion: nil)
                        }
                    }

                }
            }

            task.resume()
        }
    }
    
    func formatoShouldContinue() -> Bool{
        
        if tratoPerdida == 1{
            
            if campoComezonCueroCabelludo.text == ""{
                
                campoComezonCueroCabelludo.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoCicatrizacion.text == ""{
                
                campoCicatrizacion.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoConsultor.text == ""{
                
                campoConsultor.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }
            
            if comezonCueroCabelludoPickerView.selectedRow(inComponent: 0) == 0{
                
                if campoRascarse.text == ""{
                    
                    campoRascarse.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                    
                    return false
                }
            }
            
            if tipoCicatrizacionPickerView.selectedRow(inComponent: 0) == 2{
                
                if campoOtroCicatrizacion.text == ""{
                    
                    campoOtroCicatrizacion.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                    
                    return false
                }
            }
        }else{
            
            if campoObservoResultados.text == ""{
                
                campoObservoResultados.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoFrecuenciaLavaCabello.text == ""{
                
                campoFrecuenciaLavaCabello.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoCambiosCueroCabelludo.text == ""{
                
                campoCambiosCueroCabelludo.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoComezonCueroCabelludo.text == ""{
                
                campoComezonCueroCabelludo.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoCicatrizacion.text == ""{
                
                campoCicatrizacion.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoConsultor.text == ""{
                
                campoConsultor.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }
            
            if frecuenciaLavaCabelloPickerView.selectedRow(inComponent: 0) == 2{
                
                if campoOtroFrecuenciaLava.text == ""{
                    
                    campoOtroFrecuenciaLava.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                    
                    return false
                }
                
                if notoCambiosPickerView.selectedRow(inComponent: 0) == 0{
                    
                    if campoCuales.text == ""{
                        
                        campoCuales.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                        
                        return false
                    }else if campoHaceCuanto.text == ""{
                        
                        campoHaceCuanto.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                        
                        return false
                    }
                }
            }
            
            if comezonCueroCabelludoPickerView.selectedRow(inComponent: 0) == 0{
                
                if campoRascarse.text == ""{
                    
                    campoRascarse.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                    
                    return false
                }
            }
            
            if tipoCicatrizacionPickerView.selectedRow(inComponent: 0) == 2{
                
                if campoOtroCicatrizacion.text == ""{
                    
                    campoOtroCicatrizacion.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                    
                    return false
                }
            }
        }
        
        return true
    }
    
}
