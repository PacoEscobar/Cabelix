//
//  VistaCargaPrinipalViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/9/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class VistaCargaPrinipalViewController: UIViewController {

    var respuestaDatosUsuario: [String: Any] = [String: Any]()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (defaults.object(forKey: "tutorial") == nil){

            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let tutorialVC = storyboard.instantiateViewController(withIdentifier: "tutorialVC")
                
                tutorialVC.modalPresentationStyle = .fullScreen
                
                self.present(tutorialVC, animated: true, completion: nil)
            }
        }else{
        
            obtenerDatosUsuario(id: defaults.integer(forKey: "id"))
        }
    }
    

    func obtenerDatosUsuario(id: Int){
        
        let url = URL(string: "http://52.43.224.214/api/obtenerdatosusuario")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = "id=\(id)".data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            
            if data != nil{
                
                do{
                    
                    self.respuestaDatosUsuario = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: Any]

                    DispatchQueue.main.async {
                        
                        let defaults = UserDefaults.standard
                        
                        if(defaults.object(forKey: "id") != nil){
                            
                            let defaults = UserDefaults.standard
                            defaults.set(self.respuestaDatosUsuario["mensaje"], forKey: "mensaje_dia")
                            
                            defaults.set(self.respuestaDatosUsuario["referidos"], forKey: "referidos")
                            
                            defaults.set(self.respuestaDatosUsuario["siguienteBono"], forKey: "siguienteBono")

                            defaults.set(self.respuestaDatosUsuario["fecha_ultima_valoracion"], forKey: "fecha_ultima_valoracion")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let navegacionPrincipal = storyboard.instantiateViewController(withIdentifier: "tabBarControllerPrincipal")
                            
                            navegacionPrincipal.modalPresentationStyle = .fullScreen
                            
                            self.present(navegacionPrincipal, animated: true, completion: nil)
                        }else{
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let navegacionPrincipal = storyboard.instantiateViewController(withIdentifier: "navigationControllerNoSesion")
                            
                            navegacionPrincipal.modalPresentationStyle = .fullScreen
                            
                            self.present(navegacionPrincipal, animated: true, completion: nil)
                        }
                    }
                    
                }catch{
                    
                    print("error \(error)")
                }
            }
        }
        
        task.resume()
    }
}
