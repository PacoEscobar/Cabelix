//
//  MiCodigoDeReferidoViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/11/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class MiCodigoDeReferidoViewController: UIViewController {

    @IBOutlet weak var labelCodigo: UILabel!
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelCodigo.text = defaults.string(forKey: "codigo_afiliado")
    }
    
}
