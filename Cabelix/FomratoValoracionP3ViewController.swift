//
//  FomratoValoracionP3ViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 9/13/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class FomratoValoracionP3ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate, UITextFieldDelegate {

    
    let defaults = UserDefaults.standard
    
    var respuestas = ""
    
    var motivoConsultaPickerView: UIPickerView = UIPickerView()
    let opcionesMotivoConsulta = ["Cabello", "Barba"]
    
    var familiarSufraPerdidaPickerView: UIPickerView = UIPickerView()
    let opcionesSiNo = ["Sí", "No"]
    
    var tratoPerdidaPickerView: UIPickerView = UIPickerView()
    
    var comoTratoPerdidaPickerView: UIPickerView = UIPickerView()
    let opcionesComoTratoPerdida = ["Shampoo", "Tratamiento", "Injerto", "Tira", "Otros"]
    
    var tipoCicatrizacionPickerView: UIPickerView = UIPickerView()
    let opcionesTipoCicatrizacion = ["Normal", "Queloide", "Otra"]
    
    
    @IBOutlet weak var campoMotivoConsulta: UITextField!
    @IBOutlet weak var campoFamiliarPerdidaCabello: UITextField!
    @IBOutlet weak var campoParentesco: UITextField!
    @IBOutlet weak var campoComenzoPerderCabello: UITextField!
    @IBOutlet weak var campoTratoPerdidaCabello: UITextField!
    @IBOutlet weak var campoHaceTiempo: UITextField!
    @IBOutlet weak var campoComoTratoPerdida: UITextField!
    @IBOutlet weak var campoOtroComoTratoPerdida: UITextField!
    @IBOutlet weak var campoTipoCicatriz: UITextField!
    @IBOutlet weak var campoOtroTipoCicatriz: UITextField!
    @IBOutlet weak var campoConsultor: UITextField!
    @IBOutlet weak var botonAvanzar: UIButton!
    
    
    @IBOutlet weak var constraintComenzoPerderCabello: NSLayoutConstraint!
    @IBOutlet weak var constraintTipoCicatriz: NSLayoutConstraint!
    @IBOutlet weak var constraintConsultor: NSLayoutConstraint!
    @IBOutlet weak var constraintMotivoBottom: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){

        motivoConsultaPickerView.delegate = self
        campoMotivoConsulta.inputView = motivoConsultaPickerView
        
        familiarSufraPerdidaPickerView.delegate = self
        campoFamiliarPerdidaCabello.inputView = familiarSufraPerdidaPickerView
        
        tratoPerdidaPickerView.delegate = self
        campoTratoPerdidaCabello.inputView = tratoPerdidaPickerView
        
        comoTratoPerdidaPickerView.delegate = self
        campoComoTratoPerdida.inputView = comoTratoPerdidaPickerView
        
        tipoCicatrizacionPickerView.delegate = self
        campoTipoCicatriz.inputView = tipoCicatrizacionPickerView
        
        constraintComenzoPerderCabello.constant = 18
        constraintMotivoBottom.constant = 200
        
    }
    
    // MARK: - picker view
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == motivoConsultaPickerView{
            return opcionesMotivoConsulta.count
        }else if pickerView == comoTratoPerdidaPickerView{
            return opcionesComoTratoPerdida.count
        }else if pickerView == tipoCicatrizacionPickerView{
            return opcionesTipoCicatrizacion.count
        }else{
            return opcionesSiNo.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == motivoConsultaPickerView{
            return opcionesMotivoConsulta[row]
        }else if pickerView == comoTratoPerdidaPickerView{
            return opcionesComoTratoPerdida[row]
        }else if pickerView == tipoCicatrizacionPickerView{
            return opcionesTipoCicatrizacion[row]
        }else{
            return opcionesSiNo[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == motivoConsultaPickerView{
            
            campoMotivoConsulta.text =  opcionesMotivoConsulta[row]
            
            campoMotivoConsulta.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoMotivoConsulta.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if row == 1{
                
                campoFamiliarPerdidaCabello.isHidden = true
                campoParentesco.isHidden = true
                campoComenzoPerderCabello.isHidden = true
                campoTratoPerdidaCabello.isHidden = true
                campoHaceTiempo.isHidden = true
                campoComoTratoPerdida.isHidden = true
                campoOtroComoTratoPerdida.isHidden = true
                
                campoTipoCicatriz.isHidden = false
                
                if tipoCicatrizacionPickerView.selectedRow(inComponent: 0) == 2{
                    
                    campoOtroTipoCicatriz.isHidden = false
                    
                    constraintConsultor.constant = 66
                }else{
                    
                    constraintConsultor.constant = 18
                }
                
                campoConsultor.isHidden = false
                
                constraintTipoCicatriz.constant = 18
                
                botonAvanzar.setTitle("Finalizar", for: .normal)
            }else{
                
                campoTipoCicatriz.isHidden = true
                campoOtroTipoCicatriz.isHidden = true
                campoConsultor.isHidden = true
                
                campoFamiliarPerdidaCabello.isHidden = false
                
                if familiarSufraPerdidaPickerView.selectedRow(inComponent: 0) == 0{
                    campoParentesco.isHidden = false
                }
                
                campoComenzoPerderCabello.isHidden = false
                campoTratoPerdidaCabello.isHidden = false
                
                if tratoPerdidaPickerView.selectedRow(inComponent: 0) == 0 && campoTratoPerdidaCabello.text != ""{
                    
                    campoHaceTiempo.isHidden = false
                    campoComoTratoPerdida.isHidden = false
                }
                
                
                botonAvanzar.setTitle("Siguiente", for: .normal)
            }
        }else if pickerView == familiarSufraPerdidaPickerView{
            
            campoFamiliarPerdidaCabello.text = opcionesSiNo[row]
            campoFamiliarPerdidaCabello.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoFamiliarPerdidaCabello.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if row == 1{
                
                campoParentesco.isHidden = true
                constraintComenzoPerderCabello.constant = 18
                
               // constraintTratoPerdida.constant = 18
            }else{
                
                campoParentesco.isHidden = false
                constraintComenzoPerderCabello.constant = 66
               // constraintTratoPerdida.constant = 114
            }
        }else if pickerView == tratoPerdidaPickerView{
            
            campoTratoPerdidaCabello.text = opcionesSiNo[row]
            campoTratoPerdidaCabello.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoTratoPerdidaCabello.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if row == 1{
                
                campoHaceTiempo.isHidden = true
                campoComoTratoPerdida.isHidden = true
                campoOtroComoTratoPerdida.isHidden = true
                
            }else{
                
                campoHaceTiempo.isHidden = false
                campoComoTratoPerdida.isHidden = false
                
                if comoTratoPerdidaPickerView.selectedRow(inComponent: 0) == 4{
                    
                    campoOtroComoTratoPerdida.isHidden = false
                }
            }
        }else if pickerView == comoTratoPerdidaPickerView{

            campoComoTratoPerdida.text = opcionesComoTratoPerdida[row]
            campoComoTratoPerdida.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoComoTratoPerdida.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if row == 4{
                
                campoOtroComoTratoPerdida.isHidden = false
                
            }else{
                
                campoOtroComoTratoPerdida.isHidden = true
                
            }
        }else if pickerView == tipoCicatrizacionPickerView{
            
            campoTipoCicatriz.text = opcionesTipoCicatrizacion[row]
            campoTipoCicatriz.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
            campoTipoCicatriz.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
            
            if row == 2{
                
                campoOtroTipoCicatriz.isHidden = false
                
                constraintConsultor.constant = 66
            }else{
                
                campoOtroTipoCicatriz.isHidden = true
                
                constraintConsultor.constant = 18
            }
        }
    }
    
    // MARK: - scrollview
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        campoMotivoConsulta.resignFirstResponder()
        campoFamiliarPerdidaCabello.resignFirstResponder()
        campoParentesco.resignFirstResponder()
        campoComenzoPerderCabello.resignFirstResponder()
        campoTratoPerdidaCabello.resignFirstResponder()
        campoHaceTiempo.resignFirstResponder()
        campoComoTratoPerdida.resignFirstResponder()
        campoTipoCicatriz.resignFirstResponder()
        campoOtroTipoCicatriz.resignFirstResponder()
        campoConsultor.resignFirstResponder()
        
    }
    
    // MARK: - textfield
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == campoHaceTiempo{
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.frame.origin.y = -50
            })
        }else if textField == campoComoTratoPerdida{
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.frame.origin.y = -50
            })
        }else if textField == campoOtroComoTratoPerdida{
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.frame.origin.y = -100
            })
        }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.view.frame.origin.y = 0
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        textField.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        textField.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 0.3)
        
        return true
    }
    
    //MARK: - miselaneos
    
    @IBAction func finalizar(){
        
        respuestas = "\(respuestas)&mc=\(motivoConsultaPickerView.selectedRow(inComponent: 0))&fsp=\(familiarSufraPerdidaPickerView.selectedRow(inComponent: 0))&pare=\(campoParentesco.text ?? "")&cpc=\(campoComenzoPerderCabello.text ?? "")&tp=\(tratoPerdidaPickerView.selectedRow(inComponent: 0))&hc=\(campoHaceTiempo.text ?? "")&ctp=\(comoTratoPerdidaPickerView.selectedRow(inComponent: 0))&ctpo=\(campoOtroComoTratoPerdida.text ?? "")&tic=\(tipoCicatrizacionPickerView.selectedRow(inComponent: 0))&otic=\(campoOtroTipoCicatriz.text ?? "")&consul=\(campoConsultor.text ?? "")&cede=1"
        
        if motivoConsultaPickerView.selectedRow(inComponent: 0) == 1{
            
            
            if formatoShouldContinue(){
                
                let date = Date()
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let fecha_valoracion = dateFormatter.string(from: date)
                
                defaults.set(fecha_valoracion, forKey: "fecha_ultima_valoracion")
                                                
                let url = URL(string: "http://52.43.224.214/api/guardarvaloracion")
                var request = URLRequest(url: url!)
                request.httpMethod = "POST"
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.httpBody = respuestas.data(using: .utf8)

                let task = URLSession.shared.dataTask(with: request){ data, response, error in

                    if data != nil{

                        DispatchQueue.main.async {

                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let terminoValoracionVC = storyboard.instantiateViewController(withIdentifier: "terminoValoracionVC")

                            terminoValoracionVC.modalPresentationStyle = .fullScreen

                            self.present(terminoValoracionVC, animated: true, completion: nil)
                        }

                    }
                }

                task.resume()
            }
            
        }else{
            
            if formatoShouldContinue(){
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let formatoValoracionP4VC = storyboard.instantiateViewController(withIdentifier: "formatoValoracionP4") as! FomratoValoracionP4ViewControllerViewController
                
                formatoValoracionP4VC.respuestas = respuestas
                formatoValoracionP4VC.tratoPerdida = tratoPerdidaPickerView.selectedRow(inComponent: 0)
                
                self.navigationController?.pushViewController(formatoValoracionP4VC, animated: true)
            }
        }
    }
    
    func formatoShouldContinue() -> Bool{
        
        if campoMotivoConsulta.text == ""{
            
            campoMotivoConsulta.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            return false
        }
        
        if motivoConsultaPickerView.selectedRow(inComponent: 0) == 1{
            
            if campoTipoCicatriz.text == ""{
                
                campoTipoCicatriz.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if tipoCicatrizacionPickerView.selectedRow(inComponent: 0) == 2 && campoOtroTipoCicatriz.text == ""{
                
                campoOtroTipoCicatriz.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoConsultor.text == ""{
                
                campoConsultor.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }
        }else if motivoConsultaPickerView.selectedRow(inComponent: 0) == 0{
            
            if campoFamiliarPerdidaCabello.text == ""{
                
                campoFamiliarPerdidaCabello.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoParentesco.text == "" && familiarSufraPerdidaPickerView.selectedRow(inComponent: 0) == 0{
                
                campoParentesco.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoComenzoPerderCabello.text == ""{
                
                campoComenzoPerderCabello.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoTratoPerdidaCabello.text == ""{
                
                campoTratoPerdidaCabello.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoHaceTiempo.text == "" && tratoPerdidaPickerView.selectedRow(inComponent: 0) == 0{
                
                campoHaceTiempo.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoComoTratoPerdida.text == "" && tratoPerdidaPickerView.selectedRow(inComponent: 0) == 0{
                
                campoComoTratoPerdida.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }else if campoOtroComoTratoPerdida.text == "" && tratoPerdidaPickerView.selectedRow(inComponent: 0) == 0 && comoTratoPerdidaPickerView.selectedRow(inComponent: 0) == 4{
                
                campoOtroComoTratoPerdida.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
                
                return false
            }
        }
        
        return true
    }
}
