//
//  MensajeViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 10/5/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class MensajeViewController: UIViewController {

    var mensaje = Mensaje()
    
    @IBOutlet weak var mensajeTextField: UILabel!
    @IBOutlet weak var imagenImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mensajeTextField.text = mensaje.mensaje
        
        if mensaje.imagen != UIImage(){
            imagenImageView.image = mensaje.imagen
        }
    }

}
