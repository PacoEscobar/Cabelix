//
//  DetallesAvanceDiasViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 8/27/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class DetallesAvanceDiasViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tablaDias: UITableView!
    @IBOutlet weak var explicacionDelDia: UITextView!
    
    
    var celdaSeleccionada: IndexPath = IndexPath(row: 0, section: 0)
    let diaDelPaciente = 6
    
    let descripcionDias = ["Es el primer día, vas a estar sangrando mucho", "Es el segundo día, todavía te vas a desangrar", "Es el tercer día, vas a estar sangrando mucho", "Es el cuarto día, vas a estar sangrando mucho", "Es el quinto día, vas a estar sangrando mucho", "Este es tu sexto día, hoy tienes tu cita para remover la costra, para este día deberás notar que tu costra está agrietada, eso significa que es tiempo de extraerla.", "Es el septimo día", "Es el octavo día", "Es el noveno día", "Es el décimo día"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tablaDias.separatorStyle = .none
    }
    
    // MARK: tableview

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celda = UITableViewCell()
        
        if indexPath.row == 0{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celda_primera_DetallesAvaneDiasVC")!
            
            let indicadorDia = celda.viewWithTag(1)
            
            indicadorDia!.layer.cornerRadius = 5
            indicadorDia!.layer.masksToBounds = true
            
        }else if indexPath.row == 9{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celda_ultima_DetallesAvaneDiasVC")!
            
            let indicadorDia = celda.viewWithTag(1)
            
            indicadorDia!.layer.cornerRadius = 5
            indicadorDia!.layer.masksToBounds = true
        }else{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celda_multiple_DetallesAvaneDiasVC")!
            
            let indicadorDia = celda.viewWithTag(1)
            
            indicadorDia!.layer.cornerRadius = 5
            indicadorDia!.layer.masksToBounds = true
            
            if indexPath.row == 5{
                
                let indicadorDiaActual = celda.viewWithTag(3)
                indicadorDiaActual!.layer.cornerRadius = 10
                indicadorDiaActual!.alpha = 1.0
            }
            
            let dia = celda.viewWithTag(2) as! UILabel
            
            dia.text = "Día \(indexPath.row+1)"
        }
        
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let celda = tableView.cellForRow(at: indexPath)
        
        let indicadorDia = celda?.viewWithTag(1)
        
        indicadorDia!.backgroundColor = UIColor(red: 0.2313, green: 0.5960, blue: 0.7764, alpha: 1)
        
        if (indexPath.row + 1) == diaDelPaciente{
            
            explicacionDelDia.textColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
        }else{
            
            explicacionDelDia.textColor = UIColor(red: 0.2313, green: 0.5960, blue: 0.7764, alpha: 1)
        }
        
        explicacionDelDia.text = descripcionDias[indexPath.row]
        
        tableView.deselectRow(at: celdaSeleccionada, animated: true)
        
        celdaSeleccionada = indexPath
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let celda = tableView.cellForRow(at: indexPath)
        
        let indicadorDia = celda?.viewWithTag(1)
        
        indicadorDia!.backgroundColor = UIColor(red: 0.4980, green: 0, blue: 0.5098, alpha: 1)
    }
    
}

extension UIView {
    /// Returns the first constraint with the given identifier, if available.
    ///
    /// - Parameter identifier: The constraint identifier.
    func constraintWithIdentifier(_ identifier: String) -> NSLayoutConstraint? {
        return self.constraints.first { $0.identifier == identifier }
    }
}
