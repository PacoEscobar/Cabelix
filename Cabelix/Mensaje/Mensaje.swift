//
//  Mensaje.swift
//  Cabelix
//
//  Created by Francisco Escobar on 23/10/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class Mensaje{
    
    var id = 0
    var tieneImagen = 0
    var mensaje = ""
    var nombreImagen = ""
    var imagen = UIImage()
}
