//
//  FAQViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 8/27/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let preguntas = ["¿Cuánto es el costo por cabello?", "¿Cómo puedo pagar?", "¿Duele mucho?", "¿Dónde están ubicados?"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celda = tableView.dequeueReusableCell(withIdentifier: "celda_pregunta_FAQVC")
        
        let pregunta = celda!.viewWithTag(1) as! UILabel
        
        pregunta.text = preguntas[indexPath.row]
        
        return celda!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
