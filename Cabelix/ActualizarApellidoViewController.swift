//
//  ActualizarApellidoViewController.swift
//  Cabelix
//
//  Created by Francisco Escobar on 11/28/19.
//  Copyright © 2019 Francisco Escobar. All rights reserved.
//

import UIKit

class ActualizarApellidoViewController: UIViewController {

    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var campoApellido: UITextField!
    @IBOutlet weak var notificacionListo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func actualizarApellido(_ sender: Any) {
        
        if validarCampos() {
            
            let url = URL(string: "http://52.43.224.214/api/panel/actualizar/apellido")
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = "id=\(defaults.integer(forKey: "id"))&apellido=\(campoApellido.text ?? "-")".data(using: .utf8)
            
            let task = URLSession.shared.dataTask(with: request){ data, response, error in
                
                if data != nil{
                    
                    let respuesta = String(data: data!, encoding: .utf8)
                    
                    if respuesta == "listo"{
                        
                        DispatchQueue.main.async {
                            
                            self.defaults.set(self.campoApellido.text, forKey: "apellido")
                            
                            self.notificacionListo.isHidden = false
                        }
                    }
                    
                }
            }
            
            task.resume()
        }
    }
    
    func validarCampos() -> Bool{
        
        if campoApellido.text != ""{
            
            return true
        }else{
            
            campoApellido.backgroundColor = UIColor(red: 0.8392, green: 0.1019, blue: 0.2352, alpha: 1)
            
            return false
        }
    }
}
